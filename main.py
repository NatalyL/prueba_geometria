from ecuaciones import *


def main ():
    lodist = []

    puntos = np.array([(-2,72),(-71,13),(17,-70),(9,10),(-24,67),(73,60),(-37,-77),(54,2),(78,-15),(-41,-47),(-85,-25),(70,-28)])

    start = timeit.default_timer() # inicio tiempo de corrida
    L = GiftWrapping(puntos)

    # Puntos de la recta
    p1 = max(L[7])
    p2 = min(L[7])

    p3 = max(L[8])
    p4 = min(L[8])

    P = [p1, p2]
    Q = [p3, p4]

    # encuentra distancia del punto a la recta PQ
    for i, [x, y] in enumerate(L):
        lodist.append([distanceLtoP(x, y, P, Q), i])

    f = L[max(lodist)[1]] # punto mas lejano
    d = max(lodist)[0] # distancia maxima

    #dm = min(lodist) # distancia minima

    end = timeit.default_timer() # tiempo de corrida

    print("Distancias de la recta PQ a los puntos de la envolvente:", lodist)
    print("-------------------------------------------------------------------------------")
    print("El punto mas lejano de la envolvente con respecto a la recta PQ es: f =", f)
    print("-------------------------------------------------------------------------------")
    print("La distancia máxima de la recta PQ al punto f es:", d)
    print("-------------------------------------------------------------------------------")
    print("El area signada del triangulo PQf es:", sarea(P, Q, f))

    # Condiciones
    if sarea(P, Q, f) > 0:
        print('Como', sarea(P, Q, f), '> 0, su giro es antihorario')
    else:
        print('Como', sarea(P, Q, f), '< 0, su giro es horario')


    print("-------------------------------------------------------------------------------")
    print("La anchura del robot es:", anchura((lodist[6])[0]), "cm")

    # Condiciones
    if anchura((lodist[6])[0]) > 60:
        print('Como', anchura((lodist[6])[0]), '> 60, el robot no puede salir')
    else:
        print('Como', anchura((lodist[6])[0]), '< 60, el robot no puede salir')

    print("-------------------------------------------------------------------------------")
    print("\nTiempo de proceso: ", round(end - start, 5), "segundos")

    # Grafico de la envolvente convexa
    plt.figure()
    plt.plot(L[:, 0], L[:, 1], 'b-', pickradius=5)
    plt.plot([L[-1, 0], L[0, 0]], [L[-1, 1], L[0, 1]], 'b-', pickradius=5)
    plt.plot([p1, p3],[p2, p4], 'orange', [p1, f[0]], [p2, f[1]], 'orange',
             [p3, f[0]], [p4, f[1]], 'orange', pickradius=5) # Grafico del triangulo
    plt.plot(puntos[:, 0], puntos[:, 1], ".r")
    plt.axis('off')
    plt.show()




if __name__=='__main__':
    main()
