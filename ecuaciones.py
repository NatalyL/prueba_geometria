import sys
import numpy as np
import matplotlib.pyplot as plt
import math
import timeit

#Envolvente convexa (Jarvis)
#funciones para saber si tenemos un turno en CCW
#sentido horario
def CCW(p1,p2,p3):
    if (p3[1]-p1[1])*(p2[0]-p1[0]) <= (p2[1]-p1[1])*(p3[0]-p1[0]):
        return True
    return False

#Funciones principales
def GiftWrapping(S):
    n = len(S)
    P = [None] * n
    l = np.where(S[:, 0] == np.min(S[:, 0]))
    pointOnHull = S[l[0][0]]
    i = 0
    while True:
        P[i] = pointOnHull
        endpoint = S[0]
        for j in range(1, n):
            if (endpoint[0] == pointOnHull[0] and endpoint[1] == pointOnHull[1]) or not CCW(S[j], P[i], endpoint):
                endpoint = S[j]
        i = i + 1
        pointOnHull = endpoint
        if endpoint[0] == P[0][0] and endpoint[1] == P[0][1]:
            break
    for i in range(n):
        if P[-1] is None:
            del P[-1]
    return np.array(P)

# funcion para calcular la determinante
# determinara si un punto esta a la derecha o izquierda de una recta
def det(x, y, mi, ma):
    return ((mi[0] * ma[1]) + (x * mi[1]) + (y * ma[0]) -
            (x * ma[1]) - (ma[0] * mi[1]) - (mi[0] * y))

# funcion para calcular la distancia de una recta a un punto
def distanceLtoP(x, y, mi, ma):
    return (math.fabs(det(x, y, mi, ma)))

# Nos calcula el area signada de los punto A, B y C con esa orientación.
def sarea(A,B,C):
    return (1/2)*((B[0]-A[0])*(C[1]-A[1])-(C[0]-A[0])*(B[1]-A[1]))

# Anchura del robot
def anchura(D):
    return (D / 7) * 3





